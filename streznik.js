var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
  res.sendFile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
  res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti 
 *       (2. PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  
   // Določi novo ID številko
  var id = 0;
  for (var i in noviceSpomin) {
    id = Math.max(id, noviceSpomin[i].id);
  }
  id++;

	var naslov = req.query.naslov;
	var povzetek = req.query.povzetek;
	var kategorija = req.query.kategorija;
	var postnaStevilka = req.query.postnaStevilka;
	var kraj = req.query.kraj;
	var povezava = req.query.povezava;
  if(naslov != 0 && povzetek !=0 && kategorija != 0 && postnaStevilka !=0 && kraj != 0 && povezava != 0){
    noviceSpomin.push({id: 'id', naslov: 'naslov', povzetek: 'povzetek', kategorija: 'kategorija', postnaStevilka:'postnaStevilka', kraj: 'kraj', povezava: 'povezava'});
    res.redirect("/seznam.html");
  } else {
    res.send('Napaka pri dodajanju novice!');
    res.redirect("/seznam.html");
  }
});


app.get('/api/brisi', function(req, res) {
	var id = req.query.id;
	var uspesno = false;
	if (id != undefined) {
		for (var i in noviceSpomin) {
			if (noviceSpomin[i].id == id) {
				noviceSpomin.splice(i, 1);
				uspesno = true;
				break;
			}
		}
		if (uspesno) {
			res.redirect('/');
		} else {
			res.send('Novica z id-jem ' + id + ' ne obstaja.' + '<br/>' + 
			  '<a href="javascript:window.history.back()">Nazaj</a>');
		}
	} else {
		res.send('Napačna zahteva!');
	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Mlada leva nazaj na Češko, lastnik prepričan v zaroto',
    povzetek: 'Dva mlada levčka, ki so ju pred skoraj dvema mesecema z dovoljenjem pripeljali s Češke v Slovenjske Konjice v Mini živalski vrt, morata nazaj na Češko. Pritožbam lastnika niso ugodili, zato je sprožil upravni spor. A ta postopek vrnitve levov ne bo ustavil. Če ju lastnik ne odpelje sam, mu jih bodo na njegove stroške odvzeli in odpeljali iz Slovenije.',
    kategorija: 'novice',
    postnaStevilka: 3210,
    kraj: 'Slovenske Konjice',
    povezava: 'https://www.24ur.com/novice/slovenija/ce-bo-treba-bo-o-levih-v-slovenjskih-konicah-odlocalo-evropsko-sodisce.html'
  }, {
    id: 2,
    naslov: 'Turizem v Ljubljani: "Izgublja se raznovrstnost njenih prebivalcev"',
    povzetek: 'Čeprav se Ljubljana po številu turistov ne more primerjati z Benetkami ali Barcelono, turizem že vpliva na življenje domačinov, ki se vse bolj umikajo iz mestnega jedra.',
    kategorija: 'zabava',
    postnaStevilka: 3000,
    kraj: 'Ljubljana',
    povezava: 'http://www.rtvslo.si/slovenija/izbor-slovenija/turizem-v-ljubljani-izgublja-se-raznovrstnost-njenih-prebivalcev/463322'
  }
];
